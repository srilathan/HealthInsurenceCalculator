package com.it.emids.health.models;

public enum Gender {
	
	MALE, FEMALE, OTHERS;

}
