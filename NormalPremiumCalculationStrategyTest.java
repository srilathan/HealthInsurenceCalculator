

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;

import java.util.Set;

import org.junit.Test;

import com.it.emids.health.models.Gender;
import com.it.emids.health.models.Habbits;
import com.it.emids.health.models.Person;
import com.it.emids.health.models.PreviousHealthIssues;
import com.it.emids.health.strategies.NormalPremiumCalculationStrategy;
import com.it.emids.health.strategies.PremiumCalculationStrategy;

public class NormalPremiumCalculationStrategyTest {

	private PremiumCalculationStrategy strategy = new NormalPremiumCalculationStrategy();


	@Test
	public void testCalculatePremium_NormanGomes() throws Exception {
		Person person = new Person();
		person.setName("Norman Gomes");
		person.setGender(Gender.MALE);
		person.setAge(34);
		Set<PreviousHealthIssues> issues = person.getpreviousHealthIssues();
		issues.add(PreviousHealthIssues.Overweight);
		Set<Habbits> habbits = person.getHabbits();
		habbits.add(Habbits.Alcohol);
		habbits.add(Habbits.DailyExercise);

		double premium = strategy.calculatePremium(person);

		assertThat(premium, closeTo(6856d, 10d));
	}

	@Test
	public void testCalculatePremium_goodOldGrandma() throws Exception {
		Person person = new Person();
		person.setName("Srilatha");
		person.setGender(Gender.FEMALE);
		person.setAge(88);
		person.getHabbits().add(Habbits.DailyExercise);

		double premium = strategy.calculatePremium(person);

		assertThat(premium, closeTo(36639d, 1d));
	}

	@Test
	public void testCalculatePremium_unhealthyKid() throws Exception {
		Person person = new Person();
		person.setName("sateesh");
		person.setGender(Gender.MALE);
		person.setAge(23);
		Set<Habbits> habbits = person.getHabbits();
		habbits.add(Habbits.Drugs);
		habbits.add(Habbits.Smoking);
		habbits.add(Habbits.Alcohol);
		Set<PreviousHealthIssues> healthIssues = person.getpreviousHealthIssues();
		healthIssues.add(PreviousHealthIssues.Overweight);

		double premium = strategy.calculatePremium(person);

		assertThat(premium, closeTo(6191d, 1d));
	}


}
