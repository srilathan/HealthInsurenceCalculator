package com.it.emids.health.strategies;

import com.it.emids.health.models.Person;
import com.it.emids.health.rules.Rules;

public class NormalPremiumCalculationStrategy implements PremiumCalculationStrategy{

	// here adding base premium information
	public double calculatePremium(Person person) {
		// TODO Auto-generated method stub

		double premium = 5000;
		premium = Rules.applyAgeBasedCalcs(person, premium);
		premium = Rules.applyBadHabbitsBasedCalcs(person, premium);
		premium = Rules.applyGenderBasedCalcs(person, premium);
		premium = Rules.applyGoodHabbitsBasedCalcs(person, premium);
		premium = Rules.applyHealthIssuesBasedCalcs(person, premium);		
		return premium;

	}
	

	
}
