package com.it.emids.health.strategies;

import java.util.Set;

import com.it.emids.health.models.Gender;
import com.it.emids.health.models.Habbits;
import com.it.emids.health.models.Person;
import com.it.emids.health.models.PreviousHealthIssues;

public class TestExample {

	public static void main(String[] args) {
		
		NormalPremiumCalculationStrategy strategy= new NormalPremiumCalculationStrategy();
		
		Person person = new Person();
		person.setName("Norman Gomes");
		person.setGender(Gender.MALE);
		person.setAge(34);
		Set<PreviousHealthIssues> issues = person.getpreviousHealthIssues();
		issues.add(PreviousHealthIssues.Overweight);
		Set<Habbits> habbits = person.getHabbits();
		habbits.add(Habbits.Alcohol);
		habbits.add(Habbits.DailyExercise);
		
		
		double ss = strategy.calculatePremium(person);
		System.out.println("output is :"+ss);
	}
}
