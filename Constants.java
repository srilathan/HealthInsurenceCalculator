package com.it.emids.health.rules;

import java.util.HashSet;
import java.util.Set;

import com.it.emids.health.models.Habbits;

public interface Constants {
	
	Set<Habbits> goodHabbits = new HashSet<Habbits>() {
		{
			add(Habbits.DailyExercise);
		}
	};

	Set<Habbits> badHabbits = new HashSet<Habbits>() {
		{
			add(Habbits.Smoking);
			add(Habbits.Alcohol);
			add(Habbits.Drugs);
		}
	};


}
