package com.it.emids.health.strategies;

import com.it.emids.health.models.Person;

public interface PremiumCalculationStrategy {

	double calculatePremium(Person person);
}
