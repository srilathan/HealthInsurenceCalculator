package com.it.emids.health.models;

public enum PreviousHealthIssues {

	Hypertension, BloodPressure, BloodSugar, Overweight;
}
