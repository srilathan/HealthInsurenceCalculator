package com.it.emids.health.rules;

import com.it.emids.health.models.Gender;
import com.it.emids.health.models.Habbits;
import com.it.emids.health.models.Person;
import com.it.emids.health.models.PreviousHealthIssues;

public class Rules {

	public static double applyBadHabbitsBasedCalcs(Person person, double premium) {
		// Increase insurance premium 3% per very Bad habit
		for (Habbits habbit : person.getHabbits()) {
			if (Constants.badHabbits.contains(habbit)) {
				premium *= 1.03;
			}
		}
		return premium;
	}

	public static double applyGoodHabbitsBasedCalcs(Person person, double premium) {
		// Decrease Insurance premium 3% per every good habit
		for (Habbits habbit : person.getHabbits()) {
			if (Constants.goodHabbits.contains(habbit)) {
				premium *= 0.97;
			}
		}
		return premium;
	}

	public static double applyHealthIssuesBasedCalcs(Person person, double premium) {
		// Increase 1% per health issue
		for (PreviousHealthIssues issue : person.getpreviousHealthIssues()) {
			premium *= 1.01;
		}
		return premium;
		
	}

	public static double applyGenderBasedCalcs(Person person, double premium) {
		// Add gender specific adjustments
		if (person.getGender() == Gender.MALE) {
			premium *= 1.02;
		}
		return premium;
	}

	public static double applyAgeBasedCalcs(Person person, double premium) {
		// Increase premium decided depending on age
		if (person.getAge() >= 18) {
			premium *= 1.1;
		}
		if (person.getAge() >= 25) {
			premium *= 1.1;
		}
		if (person.getAge() >= 30) {
			premium *= 1.1;
		}
		if (person.getAge() >= 35) {
			premium *= 1.1;
		}
		if (person.getAge() >= 40) {
			// Add 20% per 5 years above 40
			int age = person.getAge() - 40;
			while (age >= 5) {
				premium *= 1.2;
				age -= 5;
			}
		}
		return premium;
	}

}
